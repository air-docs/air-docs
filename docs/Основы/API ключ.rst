API ключ
++++++++

Получение API-ключа 
===================

Для получения API-ключа достаточно воспользоваться одним из двух способов: 
создать его на платформе через интерфейс пользователя (**GUI способ**), 
или воспользоваться запросами по протоколу HTTPS (**программный способ**).


GUI-способ
----------
Перейдите на `платформу AIR <https://app.air.fail/>`_ и войдите в свой аккаунт. 
В случае отсутствия аккаунта - 
потребуется пройти через процесс регистрации. 


Перейдите на вкладку API-ключи: 

.. image:: ./apikey.png

Затем, создайте новый API ключ. Позже можно будет изменить его 
псевдоним, срок действия и корпоративную доступность.
Сам ключ изменить нельзя.

.. image:: ./apikeycreate.png

Скопируйте Ваш ключ: 

.. image:: ./apikeycopy.png

**Готово! Держите Ваш ключ в секрете.
Он предоставляет доступ к API и различным действиям с Вашим аккаунтом.**

Программный способ
------------------
Для начала Вам потребуется ``access_token`` к платформе AIR.

`Зарегистрируйте аккаунт на платформе <https://app.air.fail/register>`_, 
если Вы этого ещё не сделали. 

После регистрации, сделайте POST HTTPS-запрос на эндпойнт 
https://api.air.fail/auth/login, передав в теле запроса 
данные о Вашем аккаунте - почту в ключе ``email``, и пароль
в ключе ``password``. 

Убедитесь, что на этом этапе Вы не передаёте заголовков авторизации. 

Пример на CURL: 

.. code-block:: bash

    curl -X POST -H "Content-Type: application/json" -d '{"email": "your_email@example.com", "password": "your_password"}' https://api.air.fail/auth/login

Пример на Python: 

.. code-block:: python

    import requests

    url = "https://api.air.fail/auth/login"
    headers = {"Content-Type": "application/json"}
    data = {"email": "your_email@example.com", "password": "your_password"}
    response = requests.post(url, json=data, headers=headers)
    print(response.json())

Пример на JavaScript: 

.. code-block:: javascript

    const axios = require('axios');

    const url = 'https://api.air.fail/auth/login';
    const headers = {'Content-Type': 'application/json'};
    const data = {'email': 'your_email@example.com', 'password': 'your_password'};

    axios.post(url, data, {headers})
         .then(response => console.log(response.data))
         .catch(error => console.error(error.response.data));


В JSON-объекте ответа Вы найдёте ключ ``token``. 
Возьмите значение вложенного в ``token`` ключа ``access`` - 
это и есть Ваш ``access_token`` для платформы.

Пример начала ``access_token``: ``eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t...``

Скопируйте ``access_token`` в заголовок запроса ``Authorization``. 
В значении заголовка поставьте ключевую строку Bearer  перед 
значением токена. 

Пример заголовка: 
``Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t...``


Убедиться в корректности токена можно при помощи запроса на эндпойнт ``https://api.air.fail/auth/me``: 

Пример на CURL:

.. code-block:: bash

    curl -X GET 'https://api.air.fail/auth/me' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t...'

Пример на Python:

.. code-block:: python

    import requests

    url = "https://api.air.fail/auth/me"
    headers = {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t..."}
    response = requests.get(url, headers=headers)
    print(response.json())

Пример на JavaScript: 

.. code-block:: javascript

    const axios = require('axios');

    const url = 'https://api.air.fail/auth/me';
    const headers = {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t..."}

    axios.get(url, {headers})
         .then(response => console.log(response.data))
         .catch(error => console.error(error.response.data));

Если в ответе вернулись данные о Вашем аккаунте  - 
вы можете приступить к созданию API-ключа.

Создать API-ключ можно авторизированным POST-запросом на эндпойнт
``https://api.air.fail/public/api-key``.

Эндпойнт принимает следующие ключи в теле запроса: 

Обязательно: 

``name`` - имя для нового ключа.


Опционально: 

``expires_at`` - дата, до которой API-ключ будет считаться действующим. 
Передаётся в формате ``yyyy-mm-dd``. По умолчанию - токен не имеет ограничения по дате.

``is_business`` - является ли ключ доступным для сотрудников Вашей организации.
Передаётся в формате булева значения - ``true`` или ``false``. По умолчанию - ``false``.
Данный ключ доступен только аккаунтам с зарегистрированной организацией. 

Пример на CURL:

.. code-block:: bash

    curl -X POST 'https://api.air.fail/public/api-key' 
    -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t...'
    -H 'Content-Type: application/json' -d '{"name": "my-awesome-key"}'

Пример на Python:

.. code-block:: python

    import requests

    url = 'https://api.air.fail/public/api-key'
    headers = {
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t...',
        'Content-Type': 'application/json',
    }
    data = {'name': 'my-awesome-key'}
    
    response = requests.post(url, json=data, headers=headers)
    print(response.json())

Пример на JavaScript: 

.. code-block:: javascript

    const axios = require('axios');

    const url = 'https://api.air.fail/public/api-key';
    const headers = {
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2t...',
      'Content-Type': 'application/json',
    };
    const data = {'name': 'my-awesome-key'};
    
    axios.post(url, data, {headers})
      .then(response => console.log(response.data))
      .catch(error => console.error(error.response.data));

В ответ Вы получите JSON-объект нового созданного API ключа.
Сам ключ содержится в значении ключа ``key``.

Пример API ключа для публичного API: ``sk-B7AyeclK6tOg...``

**Готово! Держите Ваш ключ в секрете.
Он предоставляет доступ к API и различным действиям с Вашим аккаунтом.**

Использование API-ключа
=======================

Для использования эндпойнтов публичного API 
достаточно отправлять с HTTPS-запросами заголовок
``Authorization``, где значение - это Ваш публичный API токен: ``Authorization: sk-B7AyeclK6tOg...``

Для проверки работоспособности ключа можно воспользоваться запросом 
на эндпойнт: ``https://api.air.fail/public/me``.

Пример на CURL:

.. code-block:: bash

    curl -X GET 'https://api.air.fail/public/me' 
    -H 'Authorization: sk-B7AyeclK6tOg...'

Пример на Python:

.. code-block:: python

    import requests

    url = 'https://api.air.fail/public/me'
    headers = {
        'Authorization': 'sk-B7AyeclK6tOg...'
    },
    
    response = requests.get(url, headers=headers)
    print(response.json())

Пример на JavaScript: 

.. code-block:: javascript

    const axios = require('axios');

    const url = 'https://api.air.fail/public/me';
    const headers = {
      'Authorization': 'sk-B7AyeclK6tOg...',
    };
    
    axios.get(url, {headers})
      .then(response => console.log(response.data))
      .catch(error => console.error(error.response.data));

**Поздравляем! Теперь Вы готовы к использованию публичного AIR API!🥳**
