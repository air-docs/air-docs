Объект Message
++++++++++++++

Описание
========
Объект Message - универсальный контейнер для 
отправки запросов к генеративным моделям и получения генераций.

Все промпты, файлы и параметры для создания новой генерации 
инкапсулируются в этом объекте.
Пользователь отправляет к модели лишь один Message-объект, 
как тело POST запроса. 

Генеративная модель возвращает массив из генераций, 
где каждая генерация обёрнута в аналогичный объект Message.

Технически, объект Message представляет собой JSON-объект.

Ключи объекта Message
=====================

Ключи при отправке запроса
----------------------------------------------------------

``content``: текстовый промпт (входные данные) для модели.

``file``: медиа-файл (изображение, аудио, видео, документ), 
подгружаемый в качестве промпта (входных данных) для модели.

В зависимости от конкретной генеративной модели,
ключи ``content`` и ``file`` могут быть как взаимоисключающими, 
так и дополняющими друг друга. В подавляющем большинстве модели,
поддерживаемые AIR, принимают на вход именно текстовый ``content``.

``info``: вложенный JSON-объект, представляющий собой конфигурацию 
параметров для модели. Для каждой модели существует свой уникальный
набор параметров. Подробнее о создании ключа info с параметрами можно
узнать на вкладке :doc:`Параметры модели</Основы/Параметры модели>`.  

Ключи при получении ответа
--------------------------

Массив из JSON-объектов, где каждый объект имеет: 

    ``uid``: Уникальный идентификатор сообщения.

    ``content``: Текстовый ответ от модели. Либо строка, либо null.

    ``file``: Медиа-ответ от модели. Либо URL для скачивания, либо null.

    ``from_model``: Boolean-тип. ``true`` - если сообщение пришло от модели 
    (де-факто - всегда).

    ``created_at``: Время создания сообщения в формате ISO 8601: ``YYYY-MM-DDThh-mm-ss.ms+TZ``.

    ``elapsed_time``: Время, затраченное на генерацию ответа, в формате ``hh-mm-ss.ms``.

    ``is_favourite``: Boolean-тип. Было ли сообщение помечено, как любимое. По умолчанию - ``false``.

    ``is_sent``: Boolean-тип. Было ли сообщение успешно отправлено (если Вы его получили - ``true``).

    ``info``: Параметры, которые использовались при генерации ответа. По умолчанию - ``{}``, пустой JSON.
    
Пример получения генерации
==========================

Пример на CURL
----------------

Запрос: 

.. code-block:: bash

    curl -X POST 'https://api.air.fail/public/text/chatgpt' 
    -H 'Authorization: sk-...' 
    -H 'Content-Type: application/json' 
    -d '{"content": "👋", "info": {"temperature": 0.8}}'

Ответ модели: 

.. code-block:: bash

    [
        {
            "uid": "c8328e54-e936-4015-9227-469b8925914c",
            "content": "Hello! How can I assist you today?",
            "file": null,
            "from_model": true,
            "created_at": "2023-11-11T17:49:48.962314+03:00",
            "elapsed_time": "00:00:01.969683",
            "is_favourite": false,
            "is_sent": true,
            "info": {}
        }
    ]

Пример на Python
----------------

Запрос: 

.. code-block:: python

    import requests

    url = "https://api.air.fail/public/text/chatgpt"
    
    message = {"content": "👋", "info": {"temperature": 0.8}}
    headers = {"Authorization": "sk-..."}
    response = requests.post(url, headers=headers, data=message)
    
    print(response.json())

Ответ модели: 

.. code-block:: bash

    [
        {
            "uid": "c8328e54-e936-4015-9227-469b8925914c",
            "content": "Hello! How can I assist you today?",
            "file": None,
            "from_model": True,
            "created_at": "2023-11-11T17:49:48.962314+03:00",
            "elapsed_time": "00:00:01.969683",
            "is_favourite": False,
            "is_sent": True,
            "info": {}
        }
    ]

Пример на JavaScript
--------------------

Запрос: 

.. code-block:: javascript

    const axios = require('axios');

    const url = 'https://api.air.fail/public/text/chatgpt';
    const headers = {'Authorization': 'sk-...'};
    const data = {
      content: '👋',
      info: { temperature: 0.8 },
    };
    
    axios.post(url, data, { headers })
      .then(response => console.log(response.data))
      .catch(error => console.error(error.response.data));

Ответ модели: 

.. code-block:: bash

    [
        {
            "uid": "c8328e54-e936-4015-9227-469b8925914c",
            "content": "Hello! How can I assist you today?",
            "file": null,
            "from_model": true,
            "created_at": "2023-11-11T17:49:48.962314+03:00",
            "elapsed_time": "00:00:01.969683",
            "is_favourite": false,
            "is_sent": true,
            "info": {}
        }
    ]
